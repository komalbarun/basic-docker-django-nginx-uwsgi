After pulling files, 

"run docker-compose up"

to create django app, 'ssh' into running container with

"docker exec -it containerID /bin/sh", 

then cd into /var/www and do 

"django-admin startproject projectname1", You NEED to do this this the built image does not have a django app yet.

to create app, cd into created project and do 
"python manage.py startapp app name".

After that change "project" in django-docker.ini to projectname1.

final steps:

	"ctrl+c" on the terminal the services are running, "docker-compose down" and "docker-compose up". 

	You should be good to go now. I leave database config to you. Some people use
	Mongo, some use maria and others use mysql, pgsql, perconq, redis... its kind
	of a mess ^^""