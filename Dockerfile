FROM ubuntu:18.04

COPY ./r.txt r.txt

RUN apt-get -y update && \
	apt-get -y upgrade && \
	apt-get -y install uwsgi && \
	apt-get -y install python3 && \
	apt-get -y install python3-pip && \
	pip3 install -r ./r.txt && \
	ln -s /usr/bin/python3 /usr/bin/python && \
	apt-get -y install htop && \
	apt-get -y install vnstat && \
	apt-get -y install nano && \
	apt-get -y install uwsgi-plugin-python3

EXPOSE 9000

